#ifndef _ORGRULEPROVIDER_H
#define _ORGRULEPROVIDER_H
#include <iostream>
#include "RuleProvider.h"

using namespace std;

class OrgRuleProvider : public RuleProvider
{
public:
	OrgRuleProvider(string path);//tor
	~OrgRuleProvider();//dtor
	Rule readNext();//the next rule return the rule at the line
};
#endif