#include <iostream>
#include <string>
#include <vector>
#include "PacketFilter.h"

using namespace std;

class Packet{
private:
	string srcIP;
	string srcPort;
	string dstIP;
	string dstPort;
	string protocol;
public:
	Packet(string sIP, string sPort, string dIP, string dPort, string proto) : srcIP(sIP), srcPort(sPort), dstIP(dIP), dstPort(dPort), protocol(proto){}
	string getSrcIP() { return srcIP; }
	string getSrcPort() { return srcPort; }
	string getDstIP() { return dstIP; }
	string getdstPort() { return dstPort; }
	string getProtocol() { return protocol; }
	void print(){
		cout << protocol << " packet, from " << srcIP << ":" << srcPort << " to " << dstIP << ":" << dstPort;
	}
};

int main(int argc, char** argv){
	string globalProvider = "D:\\Users\\user-pc\\Desktop\\Magshimim\\C++\\FInal project\\xml_source_example.xml";
	string orgProviders[] = { "D:\\Users\\user-pc\\Desktop\\Magshimim\\C++\\FInal project\\csv_source_example.csv" };
	string userProviders[] = { "D:\\Users\\user-pc\\Desktop\\Magshimim\\C++\\FInal project\\txt_source_example.txt" };
	PacketFilter pf(globalProvider, orgProviders,1 , userProviders,1);

	vector<Packet> packets;
	packets.push_back(Packet("192.168.10.10", "80", "212.6.150.91", "80", "TCP")); //good
	packets.push_back(Packet("192.168.10.10", "21", "212.6.150.91", "50", "UDP")); //bad
	packets.push_back(Packet("192.168.10.9", "1337", "212.6.150.80", "666", "TCP")); //bad
	packets.push_back(Packet("212.6.150.90", "42", "212.6.150.91", "42", "TCP"));// good
	packets.push_back(Packet("212.6.150.90", "42", "212.6.150.91", "420", "TCP"));//bad
	packets.push_back(Packet("324.65.50.90", "5", "212.6.150.91", "42", "TCP"));// good
	packets.push_back(Packet("21.60.50.9", "777", "212.6.150.91", "42", "TCP"));// good
	packets.push_back(Packet("200.75.0.0", "4234", "212.6.150.91", "42", "TCP"));// good


	for (auto it = packets.begin(); it != packets.end(); it++){
		if (pf.filter(it->getSrcIP(), it->getSrcPort(), it->getDstIP(), it->getdstPort(), it->getProtocol())){
			it->print();
			cout << " is OK!" << endl;
		}
		else{
			it->print();
			cout << " is Blocked!" << endl;
		}
	}
	system("PAUSE");
	return 0;
}
