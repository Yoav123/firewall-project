#include "PacketFilter.h"
#include <iostream>
#include "RuleProvider.h"
#include "GlobalRuleProvider.h"
#include "UserRuleProvider.h"
#include "OrgRuleProvider.h"

using namespace std;

PacketFilter::PacketFilter(string GlobalProviderPath, string* orgProviderPath, int sizeorg, string* userProviderPath, int sizeuser) :
_GlobalProviderPath(GlobalProviderPath), _orgProviderPath(orgProviderPath), _userProviderPath(userProviderPath), _sizeorg(sizeorg),
_sizeuser(sizeuser){// All this tor is just put the path part and there array length.
	Rule a("","","","","");// Setting an empty rule/

	_providers.push_back(new GlobalRuleProvider(_GlobalProviderPath));//Pushing to the vector the Global part XML.


	/**Because this two can be array we need to use FOR to place everything**/
	for (int i = 0; i < _sizeorg; i++){
		_providers.push_back(new OrgRuleProvider(_orgProviderPath[i]));//Pushing to the vector the ORG part CSV.
	}

	for (int i = 0; i < _sizeuser; i++){
		_providers.push_back(new UserRuleProvider(_userProviderPath[i]));//Pushing to the vector the Use part TXT.
	}

	std::vector<RuleProvider*>::iterator it;//Making iterator for this so we can move throw this
	it = _providers.begin();//Setting the it to the beggining 

	if ((**it).isValid() == true){//To check if this file is Valid for work
		for (it = _providers.begin(); it != _providers.end(); ++it){//Going throw every provider.
			a = (**it).readNext();// Reading the next one aswell at the while.

			/**This while is contain that if we got to the end of the file so we stop and move on to the other provider 
			and it checks if there is empty rule (As we used at the readnext in Global User Org....) so it won't place it in the 
			Rules vector, aswell it checks if there is A rule twice or more and it's delete it.**/
			while ((**it).isEOF() == false && !(a == Rule("","","","","") && find(Rules.begin(), Rules.end(),a) == Rules.end())){ 
				Rules.push_back(a);// Pushing the Rule to the vector.
				a = (**it).readNext();// ^^
			}
		}
	}
	else
	{
		cout << (**it).getError()<<endl;//Showing the error
	}
}

bool PacketFilter::filter(string srcIP, string srcPort, string dstIP, string dstPort, string protocol){
	std::vector<Rule>::iterator it;//Setting iterator so we can move throw all the rules.
	Rule b(srcIP, srcPort, dstIP, dstPort, protocol);// This would be the "Rule" (It's actually the package).
	bool answer = true;// This is the main thing.
	for (it = Rules.begin(); it != Rules.end(); ++it){//Check if it's the same.
		if ((*it) == b){
			answer = false;
		}
	}
	return answer;
}

PacketFilter::~PacketFilter(){//Dtor...
	Rules.erase(Rules.begin(), Rules.begin());
	_providers.erase(_providers.begin(), _providers.begin());
}

