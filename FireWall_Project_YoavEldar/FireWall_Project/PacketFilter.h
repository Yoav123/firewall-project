#ifndef _PACKETFILTER_H
#define _PACKETFILTER_H
#include <iostream>
#include <vector>
#include <string>
#include "RuleProvider.h"
#include "Rule.h"
#include <algorithm>

using namespace std;

class PacketFilter
{
private:
	string* _userProviderPath;//The path array of TXT
	string* _orgProviderPath;//The path array of CSV
	string _GlobalProviderPath;//The path array of XML
	int _sizeorg;//The size of org array
	int _sizeuser; //The size of user array
	std::vector<RuleProvider*> _providers;//The all files (providers) abstract class
	std::vector<Rule> Rules;//ALL THE RULES.
public:
	PacketFilter(string GlobalProviderPath, string* orgProviderPath, int sizeorg, string* userProviderPath, int sizeuser);//tor
	~PacketFilter();//dtor
	bool filter(string srcIP, string srcPort, string dstIP, string dstPort, string protocol);//The main stuff this filter the packages.
};
#endif