#include "UserRuleProvider.h"
#include <iostream>
using namespace std;

UserRuleProvider::UserRuleProvider(string path) : RuleProvider(path){
}

UserRuleProvider::~UserRuleProvider(){}

Rule UserRuleProvider::readNext(){
	string line;//Every line gets in it
	getline(*_myFile, line);// getting the line.
	string src_IP, src_port, dst_IP, dst_port, protocol;// For every place.
	int a, b, temp, c;// Using to seperate the line parts.

	b = line.find(":");//To find the middle of the source section.
	src_IP = line.substr(0, b);

	c = line.find("	", b);//To get to the other section (Dest)
	temp = c - b;
	b++;
	temp--;
	src_port = line.substr(b, temp);

	c++;//:D (LOL!)
	b = line.find(":", c);
	temp = b - c;
	dst_IP = line.substr(c, temp);

	a = line.find("	", b);
	temp = a - b;
	b++;
	temp--;
	dst_port = line.substr(b, temp);


	c = line.find("	", a);
	c++;
	protocol = line.substr(c);

	Rule* thenewone = new Rule(src_IP, src_port, dst_IP, dst_port, protocol);
	/**cout << "src_IP: " << src_IP << " src_port: " << src_port << " dst_IP: " << dst_IP << " dst_port: " << dst_port <<
	" protocol: " << protocol << endl;**/

	return *thenewone;
}
