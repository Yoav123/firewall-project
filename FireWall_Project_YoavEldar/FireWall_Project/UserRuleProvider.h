#ifndef _USERRULEPROVIDER_H
#define _USERRULEPROVIDER_H
#include <iostream>
#include "RuleProvider.h"

class UserRuleProvider : public RuleProvider
{
public:
	UserRuleProvider(string path);//tor
	~UserRuleProvider();//dtor
	Rule readNext();//the next rule return the rule at the line
};
#endif