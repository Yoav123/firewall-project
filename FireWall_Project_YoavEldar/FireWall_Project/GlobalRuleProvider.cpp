#include "GlobalRuleProvider.h"
#include <iostream>
using namespace std;

GlobalRuleProvider::GlobalRuleProvider(string path): RuleProvider(path){
}//tor

GlobalRuleProvider::~GlobalRuleProvider(){}//dtor

Rule GlobalRuleProvider::readNext(){
	string line;//Every line gets in it
	string src_IP, src_port, dst_IP, dst_port, protocol;// For every place.
	int a, b, c, temp; // Using to seperate the line parts.
	getline(*_myFile, line);// getting the line.
	if (line == "</rules>"){//The ends of XML file ends with that so when it ends there is empty parts.
		src_IP = "";
		src_port = "";
		dst_IP = "";
		dst_port = "";
		protocol = "";
	}
	else if (line == "<rules>")//If this is the first line we don't need her so we jump to the other one.
	{
		getline(*_myFile, line);
	}
	else{
		a = line.find("source");//To find the source all the stuff is to get finally to the source.
		a = a + 8;
		b = line.find("\"", a + 1);
		c = line.find(":");
		temp = c - a;
		src_IP = line.substr(a, temp);
		//cout << src_IP << endl;//debug stuff ignore.

		temp = b - c;// Stuff to get to the source port.
		c++;
		temp--;
		src_port = line.substr(c, temp);
		//cout << src_port << endl;//debug stuff ignore.

		a = line.find("destination");//To find the destination all the stuff is to get finally to the destination.
		a = a + 13;

		b = line.find("\"", a + 1);

		c = line.find(":", a + 1);
		temp = c - a;
		dst_IP = line.substr(a, temp);
		//cout << dst_IP << endl;//debug stuff ignore.

		temp = b - c;// Stuff to get to the destination port.
		c++;
		temp--;
		dst_port = line.substr(c, temp);
		//cout << dst_port << endl;//debug stuff ignore.

		a = line.find("protocol");//To find the protocol all the stuff is to get finally to the protocol.
		a = a + 10;

		b = line.find("\"", a + 1);
		temp = b - a;
		protocol = line.substr(a, temp);
		//cout << protocol << endl;//debug stuff ignore.
	}
	Rule* thenewone = new Rule(src_IP, src_port, dst_IP, dst_port, protocol);
	/**cout << "src_IP: " << src_IP << " src_port: " << src_port << " dst_IP: " << dst_IP << " dst_port: " << dst_port <<
		" protocol: " << protocol << endl;**///debug stuff ignore.

	return *thenewone;
}
