#ifndef _RULEPROVIDER_H
#define _RULEPROVIDER_H
#include <iostream>
#include <string>
#include "Rule.h"
#include <fstream>

using namespace std;

class RuleProvider{
protected:
	string _path;//This is the address of the file.
	ifstream* _myFile = nullptr;//I puted it as protected because I use it in every class YORESHET (I've no idea how to say it in english)
private:
	string* _error;//The error.
public:
	RuleProvider(string path);//Tor gets the address and set the file.
	~RuleProvider();//Dtor.
	bool isValid();//Check if it's open.
	string getError() const;//Include the errors.
	bool isEOF() const;//Check if it's the last line.
	virtual Rule readNext() = 0;//Read the next line and actually provide the rule.
	void set();//Set it to the first line in file.
};
#endif