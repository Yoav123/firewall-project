#include <iostream>
#include "RuleProvider.h"

using namespace std;

RuleProvider::RuleProvider(string path) : _path(path){
	_myFile = new ifstream(_path);//Tor and we set here the File.
}

RuleProvider::~RuleProvider(){//Dtor, deleting the file.
	delete(_myFile);
	delete(_error);
}

bool RuleProvider::isValid(){// To see if the file is open.
	if (!(_myFile->is_open())){
		_error = new string("The file is open, can't opening it.");
		return false;
	}
	else{
		_error = nullptr;
		return true;
	}
}

string RuleProvider::getError() const{//To get the errors.
	return _error[0];
}

bool RuleProvider::isEOF() const{//This checks if we got to the end of the file (Last line)
	if (!(_myFile->eof())){
		return false;
	}
	else{
		return true;
	}
}

void RuleProvider::set(){//To set the position to the beggining.
	_myFile->seekg(0, ios::beg);
}