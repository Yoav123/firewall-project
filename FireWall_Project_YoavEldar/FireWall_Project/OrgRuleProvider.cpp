#include "OrgRuleProvider.h"
#include <iostream>
using namespace std;

OrgRuleProvider::OrgRuleProvider(string path) : RuleProvider(path){
}

OrgRuleProvider::~OrgRuleProvider(){}

Rule OrgRuleProvider::readNext(){
	string line;//Every line gets in it
	getline(*_myFile, line);// getting the line.
	int a, b, temp;// Using to seperate the line parts.
	string src_IP, src_port, dst_IP, dst_port, protocol;// For every place.

	/** It all separiad for :
	src IP || src Port || dst IP || dst Port || protocol
	       ^^          ^^        ^^          ^^
		   ||          ||        ||          ||
		   ,           ,         ,           ,
	I used this format to separiad them.**/

	a = line.find(",");
	src_IP = line.substr(0, a);
	if (src_IP == "source IP"){// To check if it the first line is the section pat of the CSV
		getline(*_myFile, line);
		a = line.find(",");
		src_IP = line.substr(0, a);
	}

	b = line.find(",", a + 1);
	temp = b - a;
	a++;
	temp--;
	src_port = line.substr(a, temp);

	a = line.find(",", b + 1);
	temp = a - b;
	b++;
	temp--;
	dst_IP = line.substr(b, temp);



	b = line.find(",", a + 1);
	temp = b - a;
	a++;
	temp--;
	dst_port = line.substr(a, temp);

	protocol = line.substr(b + 1);

	Rule* thenewone = new Rule(src_IP, src_port, dst_IP, dst_port, protocol);
	/**cout << "src_IP: " << src_IP << " src_port: " << src_port << " dst_IP: " << dst_IP << " dst_port: " << dst_port <<
	" protocol: " << protocol << endl;**/

	return *thenewone;
}
