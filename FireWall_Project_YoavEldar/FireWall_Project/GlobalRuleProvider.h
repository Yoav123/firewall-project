#ifndef _GLOBALRULEPROVIDER_H
#define _GLOBALRULEPROVIDER_H
#include <iostream>
#include "RuleProvider.h"
#include "Rule.h"

using namespace std;

class GlobalRuleProvider : public RuleProvider
{
public:
	GlobalRuleProvider(string path);//tor
	~GlobalRuleProvider();//dtor
	Rule readNext();//the next rule return the rule at the line
};
#endif