#include "Rule.h"
#include <iostream>
#include <string>

using namespace std;

Rule::Rule(string src_IP, string src_port, string dst_IP, string dst_port, string protocol) : _src_IP(src_IP), _src_port(src_port),
_dst_IP(dst_IP), _dst_port(dst_port), _protocol(protocol){}//Tor.

//This should be so we can compare between two rules or packages.
bool Rule::operator == (const Rule& other) const{
	return ((_src_IP == other.getsrcIp() || _src_IP == "any")
		&& (_src_port == other.getsrcPort() || _src_port == "any")
		&& (_dst_IP == other.getdstIp() || _dst_IP == "any")
		&& (_dst_port == other.getdstPort() || _dst_port == "any")
		&& (_protocol == other.getprotocol() || _protocol == "any")
		);
}

//I did this opeartor because I thought I will use set but finally I used vector.
bool Rule::operator < (const Rule& other) const{
	return ((_src_IP < other.getsrcIp())
		&& (_src_port < other.getsrcPort())
		&& (_dst_IP < other.getdstIp())
		&& (_dst_port < other.getdstPort())
		&& (_protocol < other.getprotocol())
		);
}

//I did this opeartor because I thought I will use set but finally I used vector.
bool Rule::operator >(const Rule& other) const{
	return ((_src_IP > other.getsrcIp())
		&& (_src_port > other.getsrcPort())
		&& (_dst_IP > other.getdstIp())
		&& (_dst_port > other.getdstPort())
		&& (_protocol > other.getprotocol())
		);
}

Rule::~Rule(){}//Dtor.

string Rule::getsrcIp() const{
	return _src_IP;//...
}
string Rule::getsrcPort() const{
	return _src_port;//...
}
string Rule::getdstIp() const{
	return _dst_IP;//...
}
string Rule::getdstPort() const{
	return _dst_port;//...
}
string Rule::getprotocol() const{
	return _protocol;//...
}